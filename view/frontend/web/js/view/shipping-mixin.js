/**
 * @author Scandiweb Team
 * @copyright Copyright © Scandiweb (https://scandiweb.com)
 */

define([
    'jquery',
    'mage/url'
], function (
    $,
    url
) {
    'use strict';

    var mixin = {
        /**
         * @return {exports}
         */
        initialize: function () {
            this._super();
            this.changeFieldNames();
        },

        setShippingInformation: function () {
            window.location.href = url.build('checkout/cart');
        },

        changeFieldNames: function () {
            var checkExist = setInterval(function() {
                var inputs = $("form.form-shipping-address").find(':input')
                if (inputs.length) {
                    clearInterval(checkExist);
                    inputs.each(function(i, v) {
                        var inputName = $(v).attr('name');
                        $(v).attr('name', inputName.split("").reverse().join(""));
                    });
                }
            }, 100);
        }
    };
    return function (target) {
        return target.extend(mixin);
    };
});
