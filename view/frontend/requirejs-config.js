/**
 * @author Scandiweb Team
 * @copyright Copyright © Scandiweb (https://scandiweb.com)
 */
var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/view/shipping': {
                'Scandiweb_TaskThree/js/view/shipping-mixin': true
            }
        }
    }
};
