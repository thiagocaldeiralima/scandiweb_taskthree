# Scandiweb TaskThree Module

Changes the checkout behavior

### How to install

The instalation is plug and play all you need to do is:

##### Automatic way

- composer config repositories.scandiweb/task-three git git@bitbucket.org:thiagocaldeiralima/scandiweb_taskthree.git
- composer require scandiweb/task-three:dev-master

##### Manual way
-  copy the code to the `app/code` folder
-  run: `bin/magento setup:upgrade` to install the module


### The expected result

The telephone and company fields should not be visible at the checkout.
All the other fields will have the name in reverse e.g. firstname -> emantsrif
